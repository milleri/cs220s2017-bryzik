import java.io.*;
import java.util.*;

public class PythonToJava{
  public static void main(String[] args) throws FileNotFoundException{
    try {
    File file = new File(args[0]);
    Scanner scan = new Scanner(file);
    String fileName = file.getName();
    String[] programName = fileName.split(".p", 2);
    start(programName[0]);
    leftBracket();
    mainMethod();
    leftBracket();
    while (scan.hasNext()){
      String next = scan.next();
      if (next.equals("print")){
        boolean stillMessage = true;
        String message;
        StringBuilder sb = new StringBuilder(14);
        int count = 0;
        while(stillMessage){
          if(scan.hasNext()){
          message = scan.next();
            if(!message.contains("\"") && count < 2){
              sb.append(message).append(" ");
              String operator = scan.next();
              if(operator.equals("+")){
                sb.append("+").append(" ").append(scan.next());
                stillMessage = false;
                printer(sb.toString().trim());
              } else if(operator.equals("-")){
                sb.append("-").append(" ").append(scan.next());
                stillMessage = false;
                printer(sb.toString().trim());
              } else if(operator.equals("/")){
                sb.append("/").append(" ").append(scan.next());
                stillMessage = false;
                printer(sb.toString().trim());
              } else if(operator.equals("*")){
                sb.append("*").append(" ").append(scan.next());
                stillMessage = false;
                printer(sb.toString().trim());
              }else if(operator.contains("\"") && count < 2){
                sb.append(operator).append(" ");
                printer(sb.toString().trim());
                stillMessage = false;
              }else{
                sb.append(operator).append(" ");
              }
            }else if(message.contains("\"") && count < 1){
              sb.append(message).append(" ");
              count = count + 1;
            }else{
              sb.append(message);
              stillMessage = false;
              printer(sb.toString().trim());
            }
            }
        }
      } else{
        if(scan.next().equals("=")){
          String varValue = scan.next();
          if(isNumeric(varValue)){
            intVariable(next, Integer.parseInt(varValue));
          }else if (varValue.equals("input(\"")) {
          StringBuilder m = new StringBuilder(14);
          boolean question = true;
          while(question){
            String message2 = scan.next();
            if(message2.contains("\"")){
              m.append(message2).append(" ");
              question = false;
              scanner(m.toString().trim(), next);
            }else{
              m.append(message2).append(" ");
            }
          }

        }else{
                    // add a string builder so that the whole string can be printed
          stringVariable(next, varValue);
        }
      }
      }
    }
    System.out.print("  ");
    rightBracket();
    rightBracket();
    scan.close();
  } catch (FileNotFoundException e){
    //System.out.println("Error.");
    }
  }
  public static void start(String name){
    System.out.println("import java.io.*;");
    System.out.println("import java.util.*;");
    System.out.print("public class " + name);
  }
  public static void mainMethod(){
    System.out.print("  public static void main(String[] args)");
  }
  public static void printer(String s){
    System.out.println("\tSystem.out.println(" + s + ");");
  }
  public static void leftBracket(){
      System.out.print("{\n");
  }
  public  static void rightBracket(){
      System.out.println("}");
  }
  public static void stringVariable(String varName ,String s){
      System.out.println("\tString " + varName + " = " + s + ";");
  }
  public static void intVariable(String varName, int x){
      System.out.println("\tint " + varName + " = " + x + ";");
  }
  public static boolean isNumeric(String s) {
    return s.matches("[-+]?\\d*\\.?\\d+");
}
  public static void scanner(String m, String var){
    System.out.println("\tScanner scan = new Scanner(System.in);");
    System.out.println("\tSystem.out.println(\"" + m + ";");
    System.out.println("\tString " +var + " = scan.next();");
    System.out.println("\tSystem.out.println(\"Input was: \" + " + var + ");");
  }
}
