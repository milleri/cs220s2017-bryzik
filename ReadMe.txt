To run this program, you must have a Test.py file or any python file
that you desire to translate. This program can only handle simple
translations which include, print statements, variable declarations (ints
and strings), adding ints and scanners.

To run this program enter the following command in the terminal window:

javac PythonToJava

java PythonToJava filename.py
